#!/bin/env node
const vm = require('vm');
const util = require('util');
const fs = require('fs');
const redis = require('redis');

var targetScript; //Script to be ran

//Build host scope 
const requestInfo = {
	boardNumber:1,
	globalNumber:0,
	program : {
		name : 'Program',
		prefix : 'prog'
	},
}

const redisWrapper = function() {
	
}

const sandbox = {
	db: {
		global : redis.createClient({
			db:requestInfo.globalNumber,
			prefix:requestInfo.boardNumber+':'+requestInfo.program.prefix
		}),
		board : redis.createClient({
			db:requestInfo.boardNumber,
			prefix:requestInfo.program.prefix
		}),
		prog : redis.createClient({
			db:requestInfo.boardNumber,
			prefix:requestInfo.program.prefix
		}),
	},
	debug : function(str) {
		console.log(util.inspect(str));
	},
	exit : function () { process.exit(); }
}

targetScript = vm.Script( fs.readFileSync( process.argv[2] ) , process.argv[2] );
targetScript.runInNewContext( sandbox , {
	displayErrors : true,
	filename: process.argv[2],
	timeout:120
});
